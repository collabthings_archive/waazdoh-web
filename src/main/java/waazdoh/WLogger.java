package waazdoh;

import java.util.logging.Level;
import java.util.logging.Logger;

public class WLogger {

	private final Object o;
	private final Logger l;

	public WLogger(Object o) {
		this.o = o;
		l = Logger.getLogger("" + o);
	}

	public static WLogger getLogger(Object o) {
		return new WLogger(o);
	}

	public void info(String string) {
		l.info(getMessage(string));
	}

	private String getMessage(String string) {
		return "" + this.o + " -- " + string;
	}

	public void error(Exception e) {
		l.log(Level.SEVERE, "Exception", e);
	}

}
