package waazdoh.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;

import com.google.appengine.api.utils.SystemProperty;

import waazdoh.WLogger;
import waazdoh.web.settings.DataStoreSettingsDAO;

public class PageComponent {
	private static final int DEFAULT_MAXAGE = 60 * 60 * 1000;
	//
	protected static DataStoreSettingsDAO settings = new DataStoreSettingsDAO();

	private Map<String, CacheFile> files = new HashMap<String, CacheFile>();
	private WLogger log = WLogger.getLogger(this);

	@Context
	private ResourceInfo resourceInfo;

	public static boolean isDevServer() {
		return SystemProperty.environment.value() == SystemProperty.Environment.Value.Development;
	}

	protected String getFile(String filepath) throws IOException {
		log.info("File path " + new File(filepath).getAbsolutePath());
		CacheFile sfile = getCacheFile(filepath);

		if (sfile == null || sfile.isOld()) {
			log.info("creating cachefile " + filepath);
			CacheFile f = newCacheFile(filepath);

			String filecontent = f.getContent();
			filecontent = replaceTags(filepath, filecontent);
			f.setContent(filecontent);
			//
			sfile = f;
		}

		return sfile.getContent();
	}

	private CacheFile getCacheFile(String filepath) {
		return files.get(filepath);
	}

	private CacheFile newCacheFile(String path) {
		CacheFile file = files.get(path);
		if (file == null) {
			file = new CacheFile(path, isDevServer() ? 1000 : DEFAULT_MAXAGE);
			files.put(path, file);
		}
		return file;
	}

	private String replaceTags(String path, String njs) {
		String tags = PageComponent.settings.getParameter("replace_" + path).getValue();
		log.info("tags " + tags);
		
		StringTokenizer st = new StringTokenizer(tags, ",");
		while (st.hasMoreTokens()) {
			String tag = st.nextToken();
			log.info("replacing tag " + tag);

			int i = njs.indexOf(tag);
			String nvalue = PageComponent.settings.getParameter(tag).getValue();

			log.info("replacing tag " + tag + " with value " + nvalue);

			njs = njs.replace(tag, "" + nvalue);
		}
		return njs;
	}

}
