/*******************************************************************************
 * Copyright (c) 2013 Juuso Vilmunen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Juuso Vilmunen - initial API and implementation
 ******************************************************************************/
package waazdoh.web.settings;

import java.io.Serializable;

import com.google.appengine.api.datastore.Entity;

public final class DSSetting implements Serializable {
	private static final long serialVersionUID = -4940057607047688043L;

	public static final String KIND = "Setting";
	//
	public static final String NAME = "name";
	public static final String VALUE = "value";
	//
	private transient Entity e;

	public DSSetting(Entity e) {
		this.e = e;
	}

	public DSSetting(String name, String value) {
		e = new Entity(DSSetting.KIND);
		setName(name);
		setValue(value);
	}

	private void setValue(final String value) {
		e.setProperty(VALUE, value);
	}

	private void setName(final String name) {
		e.setProperty(NAME, name);
	}

	public Entity getEntity() {
		return this.e;
	}

	public String getValue() {
		return (String) e.getProperty(VALUE);
	}

	public String getName() {
		return (String) e.getProperty(NAME);
	}
}
