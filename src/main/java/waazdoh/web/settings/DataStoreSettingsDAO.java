package waazdoh.web.settings;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import waazdoh.WLogger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;

public class DataStoreSettingsDAO {

	public DataStoreSettingsDAO() {
		// getParameter("start");
	}

	public Set<String> listNames() {
		Set<String> namelist = new HashSet<>();

		DatastoreService pm = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(DSSetting.KIND);
		Iterable<Entity> list = pm.prepare(q).asIterable();
		for (Entity entity : list) {
			DSSetting s = new DSSetting(entity);
			String name = s.getName();
			namelist.add(name);
		}

		return namelist;
	}

	public DSSetting getParameter(String name) {
		name = name.replace("/", "_");

		DatastoreService pm = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(DSSetting.KIND);
		q.setFilter(new FilterPredicate(DSSetting.NAME, FilterOperator.EQUAL,
				name));
		Entity e = pm.prepare(q).asSingleEntity();
		WLogger.getLogger(this).info("got " + e + " with name " + name);
		if (e != null) {
			return new DSSetting(e);
		} else {
			String defvalue = getProperty(name);
			if (defvalue == null) {
				defvalue = "default";
			}

			DSSetting ds = new DSSetting(name, defvalue);
			Transaction tr = pm.beginTransaction();
			pm.put(ds.getEntity());
			tr.commit();

			WLogger.getLogger(this).info(
					"new parameter " + name + " -> " + defvalue);

			return ds;
		}
	}

	private String getProperty(String name) {
		Properties p = new Properties();
		try {
			p.load(new FileReader("WEB-INF/defaultvalues.ini"));
			return p.getProperty(name);
		} catch (IOException e) {
			WLogger.getLogger(this).error(e);
			return null;
		}
	}

}
